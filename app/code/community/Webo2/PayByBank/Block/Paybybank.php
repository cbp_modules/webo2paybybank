<?php

/**
 *
 *                 _            ____
 * __      __ ___ | |__    ___ |___ \
 * \ \ /\ / // _ \| '_ \  / _ \  __) |
 *  \ V  V /|  __/| |_) || (_) |/ __/
 *   \_/\_/  \___||_.__/  \___/|_____|
 *
 * Webo2_PayByBank extension
 *
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the User User License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 *
 * @author          Leonidas Palaiokostas
 * @category        Webo2 Modules
 * @package         Webo2_PayByBank
 * @copyright       Copyright (c) 2017 webo2.gr
 * @license         User User License Agreement(EULA)
 *
 */
class Webo2_PayByBank_Block_Paybybank extends Mage_Payment_Block_Form
{
    
}
